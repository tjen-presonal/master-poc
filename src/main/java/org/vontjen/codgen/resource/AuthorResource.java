package org.vontjen.codgen.resource;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.vontjen.codgen.dao.AuthorRepository;
import org.vontjen.codgen.domain.Author;
import org.vontjen.codgen.model.AuthorInfo;

import java.net.URI;
import java.util.Optional;

@RestController
public class AuthorResource {

    private final AuthorRepository authorRepository;

    public AuthorResource(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @PostMapping("/authors")
    public ResponseEntity<AuthorInfo> addAuthor(@RequestBody Author author){
        Author savedAuthor = authorRepository.save(author);
        return ResponseEntity.created(URI.create("/authors/" + savedAuthor.getId())).body(toAuthhorInfo(savedAuthor));
    }

    @GetMapping("/authors/{id}")
    public ResponseEntity<AuthorInfo> getUAthor(@PathVariable String id){
        Optional<Author> author = authorRepository.findById(Long.valueOf(id));
        return author.map(aut -> ResponseEntity.ok(toAuthhorInfo(aut))).orElse(ResponseEntity.notFound().build());
    }

    private AuthorInfo toAuthhorInfo(Author savedAuthor) {
        return AuthorInfo.builder()
                .id(savedAuthor.getId())
                .name(savedAuthor.getName())
                .birthDate(savedAuthor.getBirthDate())
                .location("authors/" + savedAuthor.getId())
                .build();

    }
}
