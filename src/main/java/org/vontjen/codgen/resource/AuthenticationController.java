package org.vontjen.codgen.resource;

import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.vontjen.codgen.model.TokenRequest;
import org.vontjen.codgen.model.TokenResponse;
import org.vontjen.codgen.security.JWTTokenUtil;

import java.util.stream.Collectors;

@RestController
public class AuthenticationController {

    private final AuthenticationManager authenticationManager;

    private final JWTTokenUtil jwtTokenUtil;

    private final UserDetailsService userDetailsService;

    public AuthenticationController(AuthenticationManager authenticationManager, JWTTokenUtil jwtTokenUtil, UserDetailsService userDetailsService) {
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
        this.userDetailsService = userDetailsService;
    }


    @PostMapping("/authenticate")
    public ResponseEntity<TokenResponse> authenticate(@RequestBody TokenRequest tokenRequest) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(tokenRequest.getUsername(), tokenRequest.getPassword()));
        final UserDetails userDetails = userDetailsService.loadUserByUsername(tokenRequest.getUsername());

        String token = jwtTokenUtil.generateToken(userDetails);

        TokenResponse tokenResponse = TokenResponse
                .builder()
                .uuid(userDetails.getUsername())
                .roles(userDetails.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
                .token(token)
                .build();
        return ResponseEntity.ok(tokenResponse);
    }
}
