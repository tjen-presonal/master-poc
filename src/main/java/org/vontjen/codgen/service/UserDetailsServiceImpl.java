package org.vontjen.codgen.service;

import org.springframework.context.annotation.Primary;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collections;

@Service
@Primary
public class UserDetailsServiceImpl implements UserDetailsService {

    private final PasswordEncoder encoder;

    public UserDetailsServiceImpl(PasswordEncoder encoder) {
        this.encoder = encoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        switch (username){
            case "d1651f3e-f23b-4386-a0a4-234c933e5b42" :
                return new User("d1651f3e-f23b-4386-a0a4-234c933e5b42",
                        encoder.encode("ThisIsTheAdminPwd"),
                        Arrays.asList(new SimpleGrantedAuthority("ROLE_ADMIN"),
                                new SimpleGrantedAuthority("ROLE_USER"))
                );
            case "80f9ca2b-98a9-4358-b0b1-3f1f90f39df0" :
                return new User("80f9ca2b-98a9-4358-b0b1-3f1f90f39df0",
                        encoder.encode("12345678"),
                        Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER")));
        }
        throw new UsernameNotFoundException("User not found");
    }
}
