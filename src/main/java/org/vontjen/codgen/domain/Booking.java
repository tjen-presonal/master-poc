package org.vontjen.codgen.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.Instant;

@Data
@Entity
public class Booking {

    @Id
    @GeneratedValue
    private Long id;
    private Instant start;
    private Instant end;
    private String name;
    private String description;


}
