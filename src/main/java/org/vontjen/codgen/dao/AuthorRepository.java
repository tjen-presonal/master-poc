package org.vontjen.codgen.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.vontjen.codgen.domain.Author;

@RepositoryRestResource(exported = false)
public interface AuthorRepository extends CrudRepository<Author, Long> {
}
