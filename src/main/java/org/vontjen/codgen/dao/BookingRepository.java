package org.vontjen.codgen.dao;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.vontjen.codgen.domain.Booking;

public interface BookingRepository extends PagingAndSortingRepository<Booking, Long> {


}
