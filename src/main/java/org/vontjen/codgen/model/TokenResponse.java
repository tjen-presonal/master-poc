package org.vontjen.codgen.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class TokenResponse {

    private String uuid;
    private String token;
    private List<String> roles;
}
