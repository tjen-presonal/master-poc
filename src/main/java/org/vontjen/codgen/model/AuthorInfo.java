package org.vontjen.codgen.model;

import lombok.Builder;
import lombok.Data;

import java.time.Instant;
import java.time.ZonedDateTime;

@Data
@Builder
public class AuthorInfo {

    private Long id;

    private String name;
    private ZonedDateTime birthDate;
    private String location;

}
